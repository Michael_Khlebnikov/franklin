## Setup ##
```
// Clone the repository
git clone git@bitbucket.org:Michael_Khlebnikov/franklin.git
```
```
// Install npm
npm i
```
```
// Run default build
gulp
```
```
// URL to view the home
Local URL: http://localhost:3000
External URL: http://192.168.0.109:3000
```

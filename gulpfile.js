'use strict';

var gulp = require('gulp');
var gulpif = require('gulp-if');
var browserSync = require('browser-sync');
var svgSprite = require('gulp-svg-sprite');
var svgstore = require('gulp-svgstore');
var inject = require('gulp-inject');

// Load plugins
var $ = require('gulp-load-plugins')();


gulp.task('styles', function() {
  var browsers = [
    '> 1%',
    'last 2 versions',
    'Firefox ESR',
    'Opera 12.1'
  ];
  return gulp.src('src/assets/styles/main.less')
    .pipe($.sourcemaps.init())
    .pipe($.less({
      paths: ['bower_components'],
      'include css': true
    })
    .on('error', $.util.log))
    .pipe($.postcss([
        require('autoprefixer-core')({
          browsers: browsers
        })
      ]))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('build'))
    .pipe(browserSync.reload({stream: true}));
});



gulp.task('images', function() {
  return gulp.src('src/assets/images/**/*')
    .pipe($.imagemin({
      svgoPlugins: [{
        convertPathData: false
      }]
    }))
    .pipe(gulp.dest('build/assets/images'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts', function() {
  return gulp.src('src/assets/scripts/**/*')
    .pipe(gulp.dest('build/'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('images:svg', function () {
  return gulp.src('src/assets/icons/*.svg')
    .pipe($.svgSprite({
      svg: {
        transform: [
          (svg) => svg.replace(/(fill|fill-rule)="[#a-z0-9]+"/gi, '')
        ]
      },
      mode: {
        symbol: {
          dest: '.',
          sprite: 'icons.svg'
        },
        inline: true
      }
    }))
    .pipe(gulp.dest('src/assets/compiled'));
});

gulp.task('views', function(){
  return gulp.src([
      '!src/partials/layout.pug',
      'src/*.pug'
    ])
    .pipe($.pug({
      pretty: true
    }))
    .pipe($.inject(
      gulp.src('src/assets/compiled/icons.svg'),
      {transform: (filePath, file) => file.contents.toString('utf8')}
    ))
    .on('error', $.util.log)
    .pipe(gulp.dest('build'))
    .pipe(browserSync.reload({stream: true}));
});


gulp.task('sprite', function () {
  return gulp.src('src/assets/icons/*.png')
    .pipe($.imagemin())
    .pipe($.spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.less'
    }))
    .pipe(gulpif('*.less', gulp.dest('src/assets/compiled'), gulp.dest('./build/')))

    .pipe(browserSync.reload({stream: true}));
});


gulp.task('browser-sync', function() {
  browserSync({
    open: false,
    server: {
      baseDir: './build'
    }
  });
});


gulp.task('watch', ['build'], function() {
  gulp.watch('src/assets/styles/*.less', ['styles']);
  gulp.watch('src/assets/images/**/*', ['images']);
  gulp.watch('src/assets/scripts/*.js', ['scripts']);
  gulp.watch('src/**/*.pug', ['views']);
  gulp.watch('src/assets/icons/*.png', ['sprite']);

  gulp.start('browser-sync');
});

// JSHint grunfile.js
gulp.task('selfcheck', function() {
  return gulp.src('gulpfile.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('default'))
    .pipe($.jshint.reporter('fail'));
});


gulp.task('clean', function(cb) {
  var del = require('del');
  del(['build'], cb);
});

gulp.task('fonts', function() {
  return gulp.src('src/assets/fonts/**/*')
    .pipe(gulp.dest('build/assets/fonts'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('build', ['styles', 'scripts', 'views', 'images', 'fonts', 'images:svg', 'sprite']);


gulp.task('default', ['clean'], function() {
  gulp.start('watch');
});
